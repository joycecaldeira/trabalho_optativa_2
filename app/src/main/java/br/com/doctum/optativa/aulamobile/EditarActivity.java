package br.com.doctum.optativa.aulamobile;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditarActivity extends AppCompatActivity {

    UsuarioDB usuarioDB = new UsuarioDB(getContext());
    EditText editText;
    String nomeUser;
    int id_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        editText = (EditText) findViewById(R.id.newUser);

        Button buttonAlterar = (Button) findViewById(R.id.botaoAlterar);
        Button buttonDeletar = (Button) findViewById(R.id.botaoDeletar);

        id_user = this.getIntent().getIntExtra("id_usuario",0);

        Usuario user = usuarioDB.findById(id_user).get(0);
        editText.setText(user.getNome());

        buttonAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomeUser = editText.getText().toString();
                Usuario usuario = new Usuario();
                usuario.setNome(nomeUser);
                usuario.setId(id_user);

                usuarioDB.save(usuario);

                alert("Registro alterado!");
                Intent intent = new Intent(getApplicationContext(), ListCadastradosActivity.class);
                startActivity(intent);
                finish();
            }
        });

        buttonDeletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomeUser = editText.getText().toString();
                Usuario usuario = new Usuario();
                usuario.setNome(nomeUser);
                usuario.setId(id_user);

                usuarioDB.delete(usuario);

                alert("Registro Deletado!");
                Intent intent = new Intent(getApplicationContext(), ListCadastradosActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Context getContext() {
        return this;
    }
}
