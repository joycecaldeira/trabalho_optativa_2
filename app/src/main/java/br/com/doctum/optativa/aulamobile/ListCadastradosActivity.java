package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class ListCadastradosActivity extends AppCompatActivity {
    private static final String TAG = "cadastro";
    ListView list;
    List<Usuario> usuarios;
    UsuarioDB userBD = new UsuarioDB(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cadastrados);

        list = (ListView) findViewById(R.id.list);

        preencherLista();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Usuario user = (Usuario) usuarios.get(i);
                Intent intent = new Intent(getApplicationContext(), EditarActivity.class);

                int id = user.getId();

                Bundle params = new Bundle();
                params.putInt("id_usuario",id);
                intent.putExtras(params);
                startActivity(intent);
                finish();
            }
        });
    }

    private void preencherLista(){
        usuarios = userBD.findAll();
        ArrayAdapter<Usuario> adapter = new ArrayAdapter<Usuario>(this, android.R.layout.simple_list_item_1, usuarios);
        list.setAdapter(adapter);
    }


}
