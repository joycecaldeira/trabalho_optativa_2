package br.com.doctum.optativa.aulamobile;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ListarActivity extends AppCompatActivity {
    private static final String TAG = "services";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        ListView list = (ListView) findViewById(R.id.list);

//         this.getUsuarios(this);
        List<Aluno> alunos = this.getAlunosServer(this);

        ArrayAdapter<Aluno> adapter = new ArrayAdapter<Aluno>(this, android.R.layout.simple_list_item_1, alunos);
        list.setAdapter(adapter);

//        Button button = (Button) findViewById(R.id.buttonCep);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                EditText editText = (EditText) findViewById(R.id.campoCep);
//                getAdressByCep(getApplicationContext(), editText.getText().toString());
//            }
//        });

    }



//    public static List<Aluno> getUsuarios(Context context) {
//        try {
//            String json = readFile(context);
//            List<Aluno> alunos = parserJSON(context, json);
//            return alunos;
//        } catch (Exception e) {
//            Log.i(TAG, "Erro ao ler os json" + e.getMessage(), e);
//            return null;
//        }
//    }

    public static List<Aluno> getAlunosServer(Context context) {

        try {
            String json = new AlunoTask().execute().get();
            List<Aluno> alunos = parserJSON(context, json);
            return alunos;
        } catch (Exception e) {
            Log.i(TAG, "Erro ao ler os json" + e.getMessage(), e);
            return null;
        }
    }

    private static String loadJsonAssets(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("http://45.55.53.18/aulamobile/webservices/alunos.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private static String readFile(Context context) throws IOException {
        return loadJsonAssets(context);
    }

//    public void getAdressByCep(Context context, String cep) {
//        try {
//            String json = new CepCorreioTask(cep).execute().get();
//            CepCorreio cepCorreio = parserJSONCep(context, json);
//            TextView textView = (TextView) findViewById(R.id.endereco);
//            textView.setText(cepCorreio.getLocalidade() + "- "+ cepCorreio.getLogradouro());
//        } catch (Exception e) {
//            Log.i(TAG, "Erro ao ler o endereco " + e.getMessage(), e);
//        }
//    }
//
    private static List<Aluno> parserJSON(Context context, String json) throws IOException {
        List<Aluno> alunos = new ArrayList<Aluno>();

        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("alunos");
            JSONArray jsonAlunos = obj.getJSONArray("aluno");

            for (int i=0;i<jsonAlunos.length();i++) {
                Aluno aluno = new Aluno();
                aluno.setNome(jsonAlunos.getJSONObject(i).getString("alunos_nome"));
                aluno.setEmail(jsonAlunos.getJSONObject(i).getString("alunos_email"));
                alunos.add(aluno);
                Log.i(TAG, aluno.getNome() + " - " + aluno.getEmail());
            }
            Log.i(TAG, alunos.size() + " encontrados");


        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return alunos;
    }
//
//    private static CepCorreio parserJSONCep(Context context, String json) throws IOException {
//        CepCorreio cepCorreio = new CepCorreio();
//
//        try {
//            JSONObject root = new JSONObject(json);
//            cepCorreio.setBairro(root.getString("bairro"));
//            cepCorreio.setLogradouro(root.getString("logradouro"));
//            cepCorreio.setLocalidade(root.getString("localidade"));
//
//        } catch (JSONException e) {
//            throw new IOException(e.getMessage(), e);
//        }
//        return cepCorreio;
//    }
}
