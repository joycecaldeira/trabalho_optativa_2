package br.com.doctum.optativa.aulamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class DashboardActivity extends LifeCycle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        /*Bundle bundle = getIntent().getExtras();
        String nomeUser = bundle.getString("nome");

        TextView textView = (TextView) findViewById(R.id.textShow);
        textView.setText(nomeUser);*/

        boolean isClicked = PreferenciasUser.isCheckNotification(this);
        Log.i("click", Boolean.toString(isClicked));

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(getApplicationContext(), Configuracoes.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void selecionarOpcao(View v) {
        TextView textView = (TextView) v;

        switch(textView.getId()) {

            case R.id.iconCad:
                Log.i("click", "batman");
                Intent intent = new Intent(getApplicationContext(), CadastrarActivity.class);
                startActivity(intent);
                break;

            case R.id.iconConfig:
                Log.i("click", "luffy");
                /*Intent intent = new Intent(getApplicationContext(), CadastrarActivity.class);
                startActivity(intent);*/
                break;

            case R.id.iconEditar:
                Log.i("click", "ironman");
                Intent intent2 = new Intent(getApplicationContext(), ListCadastradosActivity.class);
                startActivity(intent2);
                break;

            case R.id.iconListar:
                Log.i("click", "darthvader");
                Intent intentList = new Intent(getApplicationContext(), ListarActivity.class);
                startActivity(intentList);
                break;
        }
    }
}
